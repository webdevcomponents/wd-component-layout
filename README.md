# wd-component-layout

**wd-component-layout** is a flexible layout based on angular-material

  - non-adaptive
  - lightweight (10.36 Kb minified, **1.63 Kb** minified+gzipped)
  - same syntax with angular-material
  - crossbrowser support (IE9+)
See more at [Angular-Material layout page](https://material.angularjs.org/HEAD/layout/introduction)

### Version
0.0.1 []()

### Installation

Using [Bower](http://bower.io/):

```sh
$ bower install git@bitbucket.org:webdevcomponents/wd-component-layout.git
```

Using Git
```sh
$ git clone git@bitbucket.org:webdevcomponents/wd-component-layout.git
```

### Todos

 - Check browser compability
 - Add examples

License
----
MIT