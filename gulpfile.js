var gulp 		= require('gulp'),
	minifyCSS	= require('gulp-cssmin'),
	rename		= require('gulp-rename');

gulp.task('default', function(){
	return gulp.src('wd-component-layout.css')
		.pipe(minifyCSS())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./'))
});